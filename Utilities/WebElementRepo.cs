namespace WebElement
{
    public class WebElements
    {
        //FIDDLE PAGE
        public static string fiddleTitle = "[alt='.NET Fiddle']";
        public static string saveButton = "#save-button";
        public static string runButton = "#run-button";
        public static string outPutWindow = "#output";

        //LOGIN MODAL
        public static string logintitle = "#login-modal-label";
        public static string emailField = "#Email";
    }
    
}