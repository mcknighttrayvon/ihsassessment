using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using WebElement;

namespace selenium
{
    [TestFixture]
    public class FiddleTests : BaseClass
    {
        [Test]
        [Category("IHSAssessment"), Description("Click run and verify the output says 'Hello World'.")]
        public void FiddleTest1()
        {
            FiddlePage fiddlePage = new FiddlePage(driver, wait);
            fiddlePage.ClickRun();

            //Verify that Hello World is the output
            Assert.True(driver.FindElement(By.CssSelector(WebElements.outPutWindow)).Text == "Hello World");
        }

        [Test]
        [Category("IHS Assessment"), Description("If your first name starts with letter 'Q-R-S-T-U' click save and verify login modal appears.")]
        public void FiddleTest2()
        {
            FiddlePage fiddlePage = new FiddlePage(driver, wait);
            fiddlePage.ClickSave();
            
            //Verify login modal has appeared
            Assert.True(driver.FindElement(By.CssSelector(WebElements.logintitle)).Displayed);
        }
    }
}
