﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace selenium
{
    public class BaseClass
    {
        public IWebDriver driver;
        public WebDriverWait wait;

        [SetUp]
        public void TestSetUp(){

            driver = new ChromeDriver("C:\\chromedriver_win32");
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl("https://dotnetfiddle.net/");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
        }
    }
}