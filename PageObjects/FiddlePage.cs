using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using WebElement;

namespace selenium
{

    public class FiddlePage : BaseClass
    {
        public FiddlePage(IWebDriver driver, WebDriverWait wait)
        {
            this.driver = driver;
            this.wait = wait;

            //Verify that the page has loaded
            wait.Until(d => driver.FindElement(By.CssSelector(WebElements.fiddleTitle)));
            Assert.True(driver.FindElement(By.CssSelector(WebElements.fiddleTitle)).Displayed);
        }

        //ACTIONS
        public void ClickSave()
        {
            driver.FindElement(By.CssSelector(WebElements.saveButton)).Click();
            wait.Until(d => driver.FindElement(By.CssSelector(WebElements.logintitle)).Displayed);
        }

        public void ClickRun()
        {
            driver.FindElement(By.CssSelector(WebElements.runButton)).Click();
        }
    }
}